import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styles: [`
    :host ::ng-deep .p-password input {
    width: 100%;
    padding:1rem;
    }
    :host ::ng-deep .pi-eye{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }
    :host ::ng-deep .pi-eye-slash{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }
  `]
})
export class LoginComponent implements OnInit, OnDestroy {

	valCheck: string[] = ['remember'];

	password: string;

	buttonEnabled: boolean = true;

	constructor(private router: Router) { }

	ngOnInit(): void { }

	ngOnDestroy(): void { }

	login() {
		this.buttonEnabled = false;
		setTimeout(() => this.router.navigate(['/dash']), 1000);
	}
}