import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad';

@Component({
	selector: 'app-sign',
	templateUrl: './sign.component.html',
	styleUrls: ['./sign.component.scss']
})
export class SignComponent {
	@ViewChild('signTab') signTab: ElementRef;
	@Output() hasSigned = new EventEmitter<string>();
	@ViewChild(SignaturePad) signaturePad: SignaturePad;

	signaturePadOptions: Object = {};

	constructor() { }

	reset() {
		this.signaturePad.set('minWidth', 5); // set szimek/signature_pad options at runtime
		this.signaturePad.set('style', 'border: 3px solid black'); // set szimek/signature_pad options at runtime
		this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
	}

	ngAfterViewInit() {
		this.signaturePadOptions = {
			'minWidth': 5,
			'canvasWidth': this.signTab.nativeElement.offsetWidth - 100,
			'canvasHeight': 300,
			'canvasStyle': "border: 2px solid red;"
		};
		// this.signaturePad is now available
		this.signaturePad.set('minWidth', 5); // set szimek/signature_pad options at runtime
		this.signaturePad.set('style', 'border: 3px solid black'); // set szimek/signature_pad options at runtime
		this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
	}

	drawComplete() {
		this.hasSigned.emit(this.signaturePad.toDataURL());
	}

}
