import { Component, OnInit } from '@angular/core';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'app-network',
	templateUrl: './network.component.html',
	styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {
	public networkStatus: boolean = true;
	networkStatus$: Subscription = Subscription.EMPTY;
	networdRefresh;
	lastConnexion;

	constructor() { }

	ngOnInit(): void {
		this.networdRefresh = setInterval(() => this.checkNetworkStatus(), 2000);
	}

	public get Status() {
		return this.networkStatus;
	}

	checkNetworkStatus() {
		this.networkStatus = navigator.onLine;
		this.networkStatus$ = merge(
			of(null),
			fromEvent(window, 'online'),
			fromEvent(window, 'offline')
		)
			.pipe(map(() => navigator.onLine))
			.subscribe(status => {
				if(status) {
					this.lastConnexion = new Date().getTime();
				}
				this.networkStatus = status;
			});
	}
}
