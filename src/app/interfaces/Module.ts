import { Student } from "./Student";

export interface Module {
    id?: string;
    name?: string;
    date?: string;
    start: string;
    end: string;
    students?: Student[];
    teachers?: string[];
    tags?: string[];
    isClosed: boolean;
}