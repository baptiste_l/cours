import { Module } from "./Module";

export interface Student {
    id?: number;
    lastName?: string;
    firstName?: string;
    signStatus?: "Absent" | "Signé" | "Attendu";
    sign?: string;
    modules?: Module[];
    comment ?: string;
}