import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main/main.component';
import { DetailsComponent } from './modules/details/details.component';
import { ListComponent } from './modules/list/list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignComponent } from './sign/sign.component';

const routes: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: 'login', component: LoginComponent },
	{ path: 'sign', component: SignComponent },
	{ path: 'dash', component: MainComponent, children: [ { path: '', component: DashboardComponent } ] },
	{
		path: 'modules', component: MainComponent,
		children: [
			{ path: 'list', component: ListComponent },
			{ path: 'details/:id', component: DetailsComponent },
		]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
