import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ConfirmationService, MessageService, PrimeIcons } from 'primeng/api';
import { Module } from 'src/app/interfaces/Module';
import { Student } from 'src/app/interfaces/Student';
import { ModuleService } from 'src/app/services/module.service';
import { OfflineService } from 'src/app/services/Offline.service';
import { SignComponent } from 'src/app/sign/sign.component';
import { StudentsComponent } from '../students/students.component';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss'],
	providers: [ConfirmationService, MessageService]
})
export class DetailsComponent implements OnInit {
	@ViewChild(SignComponent) signComponent;
	@ViewChild('qrCode') qrCodeEl: ElementRef
	comment: string;
	studentSign: string;
	breadcrumbItems = [];
	module: Module;
	customEvents: any[];
	displayQrCode: boolean = false;
	displaySign: boolean = false;
	displayAddComment: boolean = false;
	sendedSigns: boolean = false;
	showSign: boolean = false;
	qrCode: string;
	student: Student;
	qrCodeSize: number;
	displayComment: boolean = false;

	constructor(private confirmationService: ConfirmationService , public Network: OfflineService, private moduleService: ModuleService, private route: ActivatedRoute, private messageService: MessageService) { }

	ngOnInit() {
		this.breadcrumbItems.push({ label: 'Modules', routerLink: "/modules/list" });

		this.customEvents = [
            { color: '#22C55E', name: "9h00", icon: PrimeIcons.CHECK },
            { name: "10h00", color: '#22C55E', icon: PrimeIcons.CHECK },
            { name: "11h00", color: '#22C55E', icon: PrimeIcons.CHECK },
            { name: "12h00", color: '#22C55E', icon: PrimeIcons.CHECK }
        ];

		this.route.paramMap.subscribe((params: ParamMap) => {
			this.module = this.moduleService.getModule(params.get('id'));
			this.breadcrumbItems.push({ label: this.module.name });
		});
	}

	closeModule() {
        this.confirmationService.confirm({
            key: 'confirm1',
            message: '<div style="text-aligbn: center;">Voulez-vous clôturer ce module ?<br><br> Votre signature devra être apposée.</div>',
			acceptLabel: "Clôturer",
			rejectLabel: "Annuler",
			accept: () => {
				this.module.isClosed = true;
				this.moduleService.updateModule(this.module.id, this.module);
            }
        });
    }

	generateQrCode() {
		if(this.sendedSigns === false) {
			this.qrCode = "https://google.com/modules/qrcode/" + this.module.id + "/" + new Date().getTime();
			this.displayQrCode = true;
		}
	}

	sendSignMail() {
		if(this.sendedSigns === false) {
			this.messageService.add({ key: 'tst', severity: 'success', summary: 'Signatures envoyées', detail: 'Signatures envoyées aux élèves' });
			this.sendedSigns = true;
		}
	}

	storeSign(evt) {
		this.studentSign = evt;
	}

	showSignForStudent(student: Student) {
		this.student = student;
		this.showSign = !this.showSign;
	}

	saveSign() {
		this.showSign = !this.showSign;

		this.student.signStatus = "Signé";
		this.student.sign = this.studentSign;
		let studentList = this.module.students.filter(s => s.id !== this.student);

		this.module.students = studentList;
		
		this.moduleService.updateModule(this.module.id, this.module);

		this.student = null;
		this.studentSign = null;
		this.signComponent.reset();
	}

	showAddStudentComment(student) {
		this.student = student;
		if(this.student.comment != null && this.student.comment != '') {
			this.comment = this.student.comment;
		}
		this.displayAddComment = !this.displayAddComment;
	}

	showComment(student) {
		this.student = student;
		if(this.student.comment != null && this.student.comment != '') {
			this.comment = this.student.comment;
		}else {
			this.comment = "Aucun commentaire ajouté.";
		}
		this.displayComment = !this.displayComment;
	}

	saveComment() {
		this.displayAddComment = !this.displayAddComment;
		this.student.comment = this.comment;


		let studentList = this.module.students.filter(s => s.id !== this.student);

		this.module.students = studentList;
		
		this.moduleService.updateModule(this.module.id, this.module);

		this.student = null;
		this.comment = "";
	}

}
