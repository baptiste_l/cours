import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Module } from '../../interfaces/Module';
import { ModuleService } from '../../services/module.service';

@Component({
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
	modules: Module[];
	sortOptions: SelectItem[];
	sortOrder: number;
	sortField: string;
	sourceCities: any[];
	targetCities: any[];
	orderCities: any[];
	breadcrumbItems = [];

	constructor(private moduleService: ModuleService) { }

	ngOnInit() {
		this.breadcrumbItems.push({ label: 'Modules' });

		this.modules = this.moduleService.getModules();

		this.targetCities = [];

		this.sortOptions = [
			{ label: 'Du plus ancien au plus récent', value: 'date' },
			{ label: 'Du plus récent au plus ancien', value: '!date' }
		];
	}

	onSortChange(event: any) {
		const value = event.value;

		if (value.indexOf('!') === 0) {
			this.sortOrder = -1;
			this.sortField = value.substring(1, value.length);
		} else {
			this.sortOrder = 1;
			this.sortField = value;
		}
	}
}