import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Module } from '../interfaces/Module';
import { StorageService} from './Storage.service';

@Injectable({
	providedIn: 'root'
})
export class ModuleService {
	private moduleList = [];

	constructor(private http: HttpClient, private Storage: StorageService) { 
		this.http.get<any>('assets/api/modules.json').subscribe({
			next: (d: any) => {
				d.data.forEach(element => {
					this.moduleList.push(element.id);
					Storage.storeModule(element.id, element);
				});

				Storage.soreListModule(this.moduleList);
			}
		})
	}

	getModules() {
		return this.Storage.getModules();
	}

	getModule(id) {
		return this.Storage.getModule(id);
	}

	updateModule(id, module) {
		this.Storage.storeModule(id, module);
	}
}
