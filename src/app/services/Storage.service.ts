import { Injectable } from '@angular/core';
import  *  as CryptoJS from  'crypto-js';
import { Module } from '../interfaces/Module';

@Injectable({
	providedIn: 'root'
})
export class StorageService {
    private key = "W3t9sw64NBmM";
    private prefixModule = "Module-";

    /**
     * Store les id des modules;
     * 
     * @param modules tableau des id des modules
     */
    soreListModule(modules: number[]) {
        let json = JSON.stringify(modules);
        localStorage.setItem(this.prefixModule + "list", this.encrypt(json));
    }

    /**
     * Récupère la liste des modules
     * 
     * @returns module[]
     */
    getModules() {
        let data = localStorage.getItem(this.prefixModule + "list") || "";
        let json = this.decrypt(data);
        let modules = JSON.parse(json);
        let returnModule = [];
        modules.forEach(el => {
            returnModule.push(this.getModule(el));
        });

        return returnModule;
    }

    /**
     * Récupère un module
     * 
     * @param id Id du module à récupérer
     * @returns 
     */
    getModule(id: number) {
        let data = localStorage.getItem(this.prefixModule + id) || "";
        let json = this.decrypt(data);
        return JSON.parse(json);
    }

    /**
     * Store un module en local
     * 
     * @param id  id du module a stocker
     * @param module 
     */
    storeModule(id: number, module: Module) {
        let json = JSON.stringify(module);
        localStorage.setItem(this.prefixModule + id, this.encrypt(json));
    }

    /**
     * Supprimer un module
     * 
     * @param id id du mlodule a supprimer
     */
    removeModule(id: number) {
        localStorage.removeItem(this.prefixModule + id);
    }

    /**
     * Chiffre les donénes
     * 
     * @param txt donnée en clair à chiffer
     * @returns 
     */
    private encrypt(txt: string): string {
        return CryptoJS.AES.encrypt(txt, this.key).toString();
    }
    
    /**
     * Déchiffre les données
     * 
     * @param txtToDecrypt texte chiffre
     * @returns 
     */
    private decrypt(txtToDecrypt: string) {
        return CryptoJS.AES.decrypt(txtToDecrypt, this.key).toString(CryptoJS.enc.Utf8);
    }
}