import { Injectable } from "@angular/core";
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class OfflineService {
	public networkStatus: boolean = true;
	networkStatus$: Subscription = Subscription.EMPTY;
	networdRefresh;
	lastConnexion;

	constructor() {
		this.networdRefresh = setInterval(() => this.checkNetworkStatus(), 2000);
	}

	checkNetworkStatus() {
		this.networkStatus = navigator.onLine;
		this.networkStatus$ = merge(
			of(null),
			fromEvent(window, 'online'),
			fromEvent(window, 'offline')
		)
			.pipe(map(() => navigator.onLine))
			.subscribe(status => {
				if(status) {
					this.lastConnexion = new Date().getTime();
				}
				this.networkStatus = status;
			});
	}

}