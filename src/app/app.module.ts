import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './modules/list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataViewModule } from 'primeng/dataview';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {ChartModule} from 'primeng/chart';
import {ImageModule} from 'primeng/image';
import {ProgressBarModule} from 'primeng/progressbar';
import { SignaturePadModule } from 'angular2-signaturepad';
import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import timeGridPlugin from '@fullcalendar/timegrid'; 
import interactionPlugin from '@fullcalendar/interaction'; 

import { ListboxModule } from 'primeng/listbox';
import { OrderListModule } from 'primeng/orderlist';
import { RatingModule } from 'primeng/rating';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { MainComponent } from './main/main/main.component';
import { FooterComponent } from './main/footer/footer.component';
import { MenuComponent } from './main/menu/menu.component';
import { DetailsComponent } from './modules/details/details.component';
import { StudentsComponent } from './modules/students/students.component';
import { TableModule } from 'primeng/table';
import { TimelineModule } from 'primeng/timeline';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QRCodeModule } from 'angular2-qrcode';
import { ToastModule } from 'primeng/toast';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NetworkComponent } from './network/network.component';
import { LoginComponent } from './login/login.component';
import { CheckboxModule } from 'primeng/checkbox';
import { PasswordModule } from 'primeng/password';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { InputTextareaModule } from 'primeng/inputtextarea';
registerLocaleData(localeFr, 'fr');

import { DashboardComponent } from './dashboard/dashboard.component';
import { SignComponent } from './sign/sign.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    MainComponent,
    FooterComponent,
    MenuComponent,
    DetailsComponent,
    StudentsComponent,
    NetworkComponent,
    LoginComponent,
    DashboardComponent,
    SignComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    DataViewModule,
    ReactiveFormsModule,
    InputTextareaModule,
    TableModule,
    RatingModule,
    ButtonModule,
    ListboxModule,
    OrderListModule,
    DropdownModule,
    MessageModule,
    MessagesModule,
    DialogModule,
    OverlayPanelModule,
    QRCodeModule,
    ToastModule,
    BreadcrumbModule,
    CheckboxModule,
    PasswordModule,
    ConfirmDialogModule,
    FullCalendarModule,
    ChartModule,
    ImageModule,
    BrowserAnimationsModule,
    ProgressBarModule,
    SignaturePadModule,
    TimelineModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr' },
    DatePipe,    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

