import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarOptions } from '@fullcalendar/angular';
import { Module } from '../interfaces/Module';
import { ModuleService } from '../services/module.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	modules: Module[];
	signedCount: number = 0;
	totalSign: number = 0;
	missing: number = 0;
	notSignedCount: number = 0;
	curseThisWeek: number = 0;

	data: any;

	qrCode: string;

	events: any[] = [];

	calendarOptions: CalendarOptions = {
		eventClick: (t) => this.handleDateClick(t),
		locale: 'fr',
		initialView: 'timeGridWeek',
		weekends: false,
		eventColor: "red",
		slotMinTime: "08:00:00",
		slotMaxTime: "19:00:00",
		events: [],
	};

	handleDateClick(arg) {
		this.router.navigate(['/modules', 'details', arg.event.id]);
	}

	constructor(private moduleService: ModuleService, private router: Router) { }

	ngOnInit(): void {
		this.modules = this.moduleService.getModules();

		var curr = new Date; // get current date
		var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
		var last = first + 6; // last day is the first day + 6

		var firstday = new Date(curr.setDate(first));
		var lastday = new Date(curr.setDate(last));

		console.log(firstday.getTime());
		console.log(lastday);

		this.modules.forEach(m => {
			if((new Date(m.start).getTime() >= firstday.getTime() && new Date(m.start).getTime() <= lastday.getTime())) {
				this.curseThisWeek++;
			}

			this.events.push({id: m.id, title: m.name, start: m.start, end: m.end });
			this.totalSign +=  m.students.length;
			this.signedCount += m.students.filter(s => s.signStatus === "Signé").length;
			this.notSignedCount += m.students.filter(s => s.signStatus !== "Signé").length;
			this.missing += m.students.filter(s => s.signStatus === "Absent").length;
		});

		this.qrCode = "https://google.com/modules/qrcode/" + new Date().getTime();
		this.calendarOptions.events = this.events;
	}

}
