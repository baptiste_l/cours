import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MainComponent } from '../main/main.component';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
	selector: 'app-topbar',
	templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
	networkStatus: boolean = true;
	networkStatus$: Subscription = Subscription.EMPTY;
	items: MenuItem[];
	networdRefresh;

	constructor(public appMain: MainComponent) { }

	ngOnInit() {
		this.networdRefresh = setInterval(() => this.checkNetworkStatus(), 2000);
	}

	checkNetworkStatus() {
		this.networkStatus = navigator.onLine;
		this.networkStatus$ = merge(
			of(null),
			fromEvent(window, 'online'),
			fromEvent(window, 'offline')
		)
			.pipe(map(() => navigator.onLine))
			.subscribe(status => {
				this.networkStatus = status;
			});
	}
}